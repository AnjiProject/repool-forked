import time
import threading
import unittest
from unittest.mock import patch
from unittest.mock import MagicMock

from repool_forked import ConnectionPool, PoolException


@patch('repool_forked.pool.R')
class TestPool(unittest.TestCase):

    def _start_test(self, mock_r, pool_size=10):
        mock_r.connect = MagicMock()
        mock_r.close = MagicMock()
        p = ConnectionPool({}, pool_size=pool_size)
        p.init_pool()
        return p

    def test_create_pool(self, mock_r):
        p = self._start_test(mock_r)
        self.assertFalse(p.empty())
        p.release_pool()

    def test_acquire_release_one(self, mock_r):
        p = self._start_test(mock_r)
        nb_init = p._pool.qsize()
        conn = p.acquire()
        p.release(conn)
        nb_term = p._pool.qsize()
        self.assertEqual(nb_init, nb_term)
        p.release_pool()

    def test_acquire_one(self, mock_r):
        p = self._start_test(mock_r)
        nb_init = p._pool.qsize()
        conn = p.acquire()
        nb_term = p._pool.qsize()
        self.assertEqual(nb_init - 1, nb_term)
        p.release(conn)
        p.release_pool()

    def test_acquire(self, mock_r):
        p = self._start_test(mock_r)
        conn = p.acquire()
        self.assertIsInstance(conn, MagicMock)
        p.release(conn)
        p.release_pool()

    def test_release_pool(self, mock_r):
        p = self._start_test(mock_r)
        conn1 = p.acquire()
        _ = p.acquire()
        p.release(conn1)
        with self.assertRaises(PoolException):
            p.release_pool()

    def test_connect(self, mock_r):
        p = self._start_test(mock_r, pool_size=1)
        with p.connect() as conn:
            self.assertEqual(0, p._pool.qsize())
        self.assertEqual(1, p._pool.qsize())
        p.release_pool()

    def test_connect_and_wait(self, mock_r):
        p = self._start_test(mock_r, pool_size=1)

        def acquire_and_wait():
            with p.connect() as _:
                time.sleep(3)

        t1 = threading.Thread(target=acquire_and_wait, daemon=True)
        t1.start()
        with p.connect() as _:
            self.assertEqual(0, p._pool.qsize())
        p.release_pool()
