# Copyright (C) 2014  Nicolas Jouanin
#
# This file is part of Repool.
#
# Repool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Repool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Repool.  If not, see <http://www.gnu.org/licenses/>.


from setuptools import setup, find_packages

setup(
    name="repool_forked",
    version="1.0.0",
    description="Connection pool for rethinkdb",
    author="Bogdan Gladyshev",
    author_email='siredivn.dark@gmail.com',
    url="https://gitlab.com/AnjiProject/repool-forked",
    license='GPLv3',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'rethinkdb>=2.4.0',
        'tenacity>=5.0.0'
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Database'
    ]
)
