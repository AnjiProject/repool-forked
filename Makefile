lint:
	pylint repool_forked
	pycodestyle repool_forked
	mypy --ignore-missing-imports repool_forked
inspect:
	pipenv check
	bandit -r repool_forked    
pytest:
	pytest tests