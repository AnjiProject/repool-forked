# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2019-03-03

### Changed

- Migrate to `rethinkdb>=2.4.0`

## [0.3.1] - 2018-05-30

### Added

- Looped pool mode
- Retry after access to pool

### Changed

- Cleanup logic now works when you take connection

### Removed

- Cleanup thread

[1.0.0]: hthttps://gitlab.com/AnjiProject/repool-forked/compare/v0.3.1...v1.0.0
[0.3.1]: hthttps://gitlab.com/AnjiProject/repool-forked/compare/v0.3.0...v0.3.1