RethinkDB connection pool
=========================

`repool-forked` is a Python library which provides a connection pool management for accessing a [RethinkDB](http://rethinkdb.com/) database. `repool-forked` creates and maintains a configurable pool of active connection to a RethinkDB database. These connections are then available individually through a basic API.

Internally, `repool-forked` uses the Python [Queue](https://docs.python.org/3.4/library/queue.html) class which is thread-safe. This means that the same connection pool can be share between several threads.


Installation
------------

`repool-forked` is available as a python library on [Pypi](https://pypi.python.org/pypi/repool). Installation is very simple using pip :

    $ pip install repool-forked

This will install `repool-forked` as well as `rethinkdb` dependency.


Basic usage
-----------

A new connection pool using default connection configurations (`host="localhost", port=28015, db="test"`) can simply be created by :

    from repool import ConnectionPool

    pool = ConnectionPool()
    conn = pool.acquire()         #returns a Connection instance
    r.table('heroes').run(conn)   #do RethinkDB stuff
    # ...
    pool.release(conn)          #put back connection to the pool
    pool.release_pool()         #release pool (close rethinkdb connections)

    # ...
    with pool.connect() as conn1:
        # do something with conn1
    # pool.release(conn1) is automatically called after leaving the with code block


Connections, that used for changes() api
----------------------------------------

This connection will be used forever, so, for this connection is required to use special syntax:

```
    with pool.connect(looped_mode=True) as conn1:
        # do something with conn1 
```


Optional arguments
------------------

`ConnectionPool` creation accepts a number of optional arguments :
* `rethinkdb_connection_kwargs` : dict with keys, which corresponds to rethinkdb [connect()](http://rethinkdb.com/api/python/#connect) method.
* `pool_size` : set the pool size, ie. the number of connections opened simultaneously (default=10).
* `connection_ttl` : set the connection time to live. Connections older than TTL are automatically closed and re-opened by an internal thread (default=3600 seconds, set to 0 for disable)
